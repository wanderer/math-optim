// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"log"
)

func main() {
	err := run()
	if err != nil {
		if err.Error() == "ErrNoAlgoSelected" {
			return
		}

		log.Fatal(err)
	}
}
