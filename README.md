# [`math-optim`](https://git.dotya.ml/wanderer/math-optim)

> solving a few mathematical optimisation tasks using well-known algorithms
> (originally a semestral project for the Mathematical Informatics course)

[![built with nix](./.badges/built-with-nix.svg)](https://builtwithnix.org)
[![Build Status](https://drone.dotya.ml/api/badges/wanderer/math-optim/status.svg?ref=refs/heads/development)](https://drone.dotya.ml/wanderer/math-optim)
[![Go Report Card](https://goreportcard.com/badge/git.dotya.ml/wanderer/math-optim)](https://goreportcard.com/report/git.dotya.ml/wanderer/math-optim)
[![Go Documentation](./.badges/godoc.svg)](https://godocs.io/git.dotya.ml/wanderer/math-optim)

feel free to peek at the *sawce*.

### example math-optim run
[![asciicast](https://asciinema.org/a/518391.svg)](https://asciinema.org/a/518391)

### LICENSE
GPL-3.0-or-later (see [LICENSE](LICENSE) for details).
