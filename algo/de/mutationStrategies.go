// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package de

const (
	// DEBest1Exp is the DE/best/1/exp strategy.
	DEBest1Exp int = iota
	// DERand1Exp is the DE/rand/1/exp strategy.
	DERand1Exp
	// DERandtoBest1Exp is the DE/rand-to-best/1/exp strategy.
	DERandtoBest1Exp
	// DEBest2Exp is the DE/best/2/exp strategy.
	DEBest2Exp
	// DERand2Exp is the DE/rand/2/exp strategy.
	DERand2Exp

	// binomial cross-over strategies.

	// DEBest1Bin is the DE/best/1/bin strategy.
	DEBest1Bin
	// DERand1Bin is the DE/rand/1/bin strategy.
	DERand1Bin
	// DERandtoBest1Bin is the DE/rand-to-best/1/bin strategy.
	DERandtoBest1Bin
	// DEBest2Bin is the DE/best/2/bin strategy.
	DEBest2Bin
	// DERand2Bin is the DE/rand/2/bin strategy.
	DERand2Bin

	// 3-wide changes.

	// DEBest3Exp is the DE/best/3/exp strategy.
	DEBest3Exp
	// DEBest3Bin is the DE/best/3/bin strategy.
	DEBest3Bin
	// DERand3Exp is the DE/rand/3/exp strategy.
	DERand3Exp
	// DERand3Bin is the DE/rand/3/bin strategy.
	DERand3Bin

	// DERandtoCurrent2E is the DE/rand-to-current/2/exp strategy.
	DERandtoCurrent2Exp
	// DERandtoCurrent2Bin is the DE/rand-to-current/2/bin strategy.
	DERandtoCurrent2Bin

	// DERandtoBestandCurrent2Exp is the DE/rand-to-best-and-current/2/exp strategy.
	DERandtoBestandCurrent2Exp
	// DERandtoBestandCurrent2Bin is the DE/rand-to-best-and-current/2/bin strategy.
	DERandtoBestandCurrent2Bin
)
