// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

// Package de contains implementation details of Differential Evolution kind of
// algorithms.
package de
