// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package ga

func (p *SOMAT3APopulation) actualise(migrants []int) {
	f := p.ProblemFunc

	for _, i := range migrants {
		bestJump := p.Population[i].Jumps[0]

		for j := 1; j < p.Njumps; j++ {
			jump := p.Population[i].Jumps[j]

			// incrementing fes for the following is done in
			// somat3aMigration.go.
			if f(jump) < f(bestJump) {
				bestJump = jump
			}
		}

		if f(bestJump) < f(p.Population[i].CurX) {
			p.Population[i].CurX = bestJump
		}

		p.fes += 2

		// clean jump history
		p.Population[i].Jumps = nil
	}
}
