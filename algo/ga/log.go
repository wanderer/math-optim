// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package ga

import (
	"log"
	"os"
)

// gaLogger declares and initialises a "custom" ga logger.
var gaLogger = log.New(os.Stderr, " *** Γ ga:", log.Ldate|log.Ltime|log.Lshortfile)

// newLogger can be used to get a logger with a custom prefix.
func newLogger(prefix string) *log.Logger {
	return log.New(os.Stderr, prefix, log.Ldate|log.Ltime|log.Lshortfile)
}
