// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package ga

type gaError struct {
	s string
}

var (
	errGenerationsIsZero    = newGAError("Generations cannot be 0")
	errBenchMinItersIsLTOne = newGAError("Minimum bench iterations cannot be less than 1")
	errNPIsZero             = newGAError("NP cannot be 0")
	errKIsZero              = newGAError("K cannot be 0")
	errMIsZero              = newGAError("M cannot be 0")
	errNIsZero              = newGAError("N cannot be 0")
	errNjumpsIsZero         = newGAError("Njumps cannot be 0")
	errDimensionsUnset      = newGAError("Dimensions cannot be unset")
	errBenchUnset           = newGAError("Bench cannot be unset")
)

func (e *gaError) Error() string {
	return e.s
}

func newGAError(text string) error {
	return &gaError{s: text}
}
