// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

// Package algo provides implementations of optimisation algorithms.
package algo
