// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package algo

import (
	"log"
	"os"
	"sync"
	"testing"

	"git.dotya.ml/wanderer/math-optim/report"
)

var wg sync.WaitGroup

var m sync.Mutex

func TestDoRandomSearchExec(t *testing.T) {
	t.Parallel()

	wg.Add(1)

	// use t.tmpdir
	go DoRandomSearch(&wg, &m)

	wg.Wait()

	picsDir := report.GetPicsDir() + "-test-rs"

	// attempt to clean up.
	if err := os.RemoveAll(picsDir); err != nil {
		t.Error(err)
	}

	// if `stat` on picsDir succeeds now, something is wrong (namely the
	// clean-up apparently did not succeed).
	if _, err := os.Stat(picsDir); err == nil {
		t.Error("picsDir should have already been cleaned up")
	}

	log.Println("pwd:", os.Getenv("PWD"))
	// clean up outdir.
	if err := os.RemoveAll("out"); err != nil {
		t.Error(err)
	}
}

func TestDoSHCExec(t *testing.T) {
	t.Parallel()

	wg.Add(1)

	go DoStochasticHillClimbing(&wg, &m)

	wg.Wait()

	picsDir := report.GetPicsDir() + "-test-shc"

	// attempt to clean up.
	if err := os.RemoveAll(picsDir); err != nil {
		t.Error(err)
	}

	// if `stat` on picsDir succeeds now, something is wrong (namely the
	// clean-up apparently did not succeed).
	if _, err := os.Stat(picsDir); err == nil {
		t.Error("picsDir should have already been cleaned up")
	}

	log.Println("pwd:", os.Getenv("PWD"))
	// clean up outdir.
	if err := os.RemoveAll("out"); err != nil {
		t.Error(err)
	}
}
