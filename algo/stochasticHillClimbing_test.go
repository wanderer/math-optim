// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package algo

import "testing"

func TestFmtSHCOut(t *testing.T) {
	want := " ***  stochastic hill climbing: oh what a mountain that is!"
	got := fmtSHCOut("oh what a mountain that is!")

	if want != got {
		t.Errorf("strings do not equal, want: %q, got: %q", want, got)
	}
}

func TestPrintSHC(t *testing.T) {
	printSHC("whatever")
}

// nolint: ifshort
func TestGetBenchVariance_Schwefel(t *testing.T) {
	benchName := "Schwefel"
	want := 1000.0
	got := getBenchSearchSpaceSize(benchName)

	if want != got {
		t.Errorf(
			"wrong bench variance for %q, want: %f, got: %f",
			benchName, want, got,
		)
	}
}

// nolint: ifshort
func TestGetBenchVariance_DeJong1st(t *testing.T) {
	benchName := "De Jong 1st"
	want := 10.0
	got := getBenchSearchSpaceSize(benchName)

	if want != got {
		t.Errorf(
			"wrong bench variance for %q, want: %f, got: %f",
			benchName, want, got,
		)
	}
}

// nolint: ifshort
func TestGetBenchVariance_DeJong2nd(t *testing.T) {
	benchName := "De Jong 2nd"
	want := 10.0
	got := getBenchSearchSpaceSize(benchName)

	if want != got {
		t.Errorf(
			"wrong bench variance for %q , want: %f, got: %f",
			benchName, want, got,
		)
	}
}

func TestGetAllowedTweak(t *testing.T) {
	// based on the setting of bench.MaxNeighbourVariance being 10, as in 10%
	// of the search total search space. the allowedTweak value is therefore
	// those 10% in "exact" terms.
	benchSearchSpaceSize := 1000.0
	want := 100.0
	got := getAllowedTweak(benchSearchSpaceSize)

	if want != got {
		t.Errorf("wrong allowedTweak (benchSearchSpaceSize: %f), want: %f, got: %f", benchSearchSpaceSize, want, got)
	}

	benchSearchSpaceSize = 100.0
	want = 10.0
	got = getAllowedTweak(benchSearchSpaceSize)

	if want != got {
		t.Errorf("wrong allowedTweak (benchSearchSpaceSize: %f), want: %f, got: %f", benchSearchSpaceSize, want, got)
	}
}
