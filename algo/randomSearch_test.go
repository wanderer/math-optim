// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package algo

import "testing"

func TestFmtRandomSearchOut(t *testing.T) {
	want := " ***  random search: this is so random..."
	got := fmtRandomSearchOut("this is so random...")

	if want != got {
		t.Errorf("strings do not equal, want: %q, got: %q", want, got)
	}
}

func TestPrintRandomSearch(t *testing.T) {
	printRandomSearch("whatever")
}
