// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package bench

import (
	"testing"
)

// nolint: ifshort
func TestMin_funcParams(t *testing.T) {
	fP := funcParams{min: 0.123, max: 123.13432}
	want := 0.123
	got := fP.Min()

	if want != got {
		t.Errorf("wrong min, want: %f, got: %f", want, got)
	}
}

// nolint: ifshort
func TestMax_funcParams(t *testing.T) {
	fP := funcParams{min: 87534104.143252, max: 43966223.521849}
	want := 43966223.521849
	got := fP.Max()

	if want != got {
		t.Errorf("wrong max, want: %f, got: %f", want, got)
	}
}

// at the risk of testing implementation detail, these specific values are
// pre-set boundaries of the respective bench functions and as such are
// arguably important for the package/project.

// nolint: ifshort
func TestSchwefelParams(t *testing.T) {
	want := funcParams{min: -500.0, max: 500.0}
	got := SchwefelParams

	if want != got {
		t.Errorf("wrong SchwefelParams, want: %+v, got: %+v", want, got)
	}
}

// nolint: ifshort
func TestDeJong1Params(t *testing.T) {
	want := funcParams{min: -5.0, max: 5.0}
	got := DeJong1Params

	if want != got {
		t.Errorf("wrong DeJong1Params, want: %+v, got: %+v", want, got)
	}
}

// nolint: ifshort
func TestDeJong2Params(t *testing.T) {
	want := funcParams{min: -5.0, max: 5.0}
	got := DeJong2Params

	if want != got {
		t.Errorf("wrong DeJong2Params, want: %+v, got: %+v", want, got)
	}
}

func TestGetGAMaxFES(t *testing.T) {
	dim := 3
	want := 15000
	got := GetGAMaxFES(dim)

	if want != got {
		t.Errorf("wrong GAMaxFES result, want: %d, got: %d", want, got)
	}
}
