// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package cec2020

import (
	"testing"
)

func TestLoadInputData(t *testing.T) {
	dim := 10
	fnum := 1
	m, s := LoadInputData(dim)
	wantM := -0.6013070130189602
	gotM := m[fnum][0][0]

	// test matrix data.
	if wantM != gotM {
		t.Errorf("unexpected result, want: %v, got: %v", wantM, gotM)
	}

	wantM = 9.2845625214477390e-01
	gotM = m[fnum][1][1]

	if wantM != gotM {
		t.Errorf("unexpected result, want: %v, got: %v", wantM, gotM)
	}

	// test shiftData.
	wantS := -70.42955971808618
	gotS := s[fnum][0][1]

	if wantS != gotS {
		t.Errorf("unexpected result, want: %v, got: %v", wantS, gotS)
	}

	wantS = 36.645777950364064
	gotS = s[fnum][0][10]

	if wantS != gotS {
		t.Errorf("unexpected result, want: %v, got: %v", wantS, gotS)
	}

	// test fnum 24 100D.
	fnum = 24
	dim = 100
	m, s = LoadInputData(dim)

	wantM = 0.5743672086265577
	gotM = m[fnum][613][99]

	if wantM != gotM {
		t.Errorf("unexpected result, want: %v, got: %v", wantM, gotM)
	}

	wantS = 73.24860442860452
	gotS = s[fnum][7][89]

	if wantS != gotS {
		t.Errorf("unexpected result, want: %v, got: %v", wantS, gotS)
	}
}
