// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package cec2020

import "testing"

func TestComposition1(t *testing.T) {
	in := []float64{-9.2, 10, .7, 3, -80.2843}
	want := 7248.007126969505

	got := Composition1(in)

	if got != want {
		t.Errorf("unexpected result from Composition1, want: %f, got: %v", want, got)
	}
}

func TestComposition2(t *testing.T) {
	in := []float64{-9.2, 10, .7, 3, -80.2843}
	want := 2.0384259701506793e+08

	got := Composition2(in)

	if got != want {
		t.Errorf("unexpected result from Composition2, want: %f, got: %v", want, got)
	}
}

func TestComposition3(t *testing.T) {
	in := []float64{-9.2, 10, .7, 3, -80.2843}
	want := 8.699499674241544e+07

	got := Composition3(in)

	if got != want {
		t.Errorf("unexpected result from Composition3, want: %f, got: %v", want, got)
	}
}

func TestGriewank(t *testing.T) {
	in := []float64{-9.2, 10, .7, 3, -80.2843}
	want := 2.6599247066225002

	got := Griewank(in)

	if got != want {
		t.Errorf("unexpected result from Griewank, want: %f, got: %v", want, got)
	}
}

func TestHybrid1(t *testing.T) {
	in := []float64{-9.2, 10, .7, 3, -80.2843}
	want := 2.0385039193698734e+08

	got := Hybrid1(in)

	if got != want {
		t.Errorf("unexpected result from Hybrid1, want: %f, got: %v", want, got)
	}
}

func TestHybrid2(t *testing.T) {
	in := []float64{-9.2, 10, .7, 3, -80.2843}
	want := 4.6901057350488175e+06

	got := Hybrid2(in)

	if got != want {
		t.Errorf("unexpected result from Hybrid2, want: %f, got: %v", want, got)
	}
}

func TestHybrid3(t *testing.T) {
	in := []float64{-9.2, 10, .7, 3, -80.2843}
	want := 2.0618485725498265e+08

	got := Hybrid3(in)

	if got != want {
		t.Errorf("unexpected result from Hybrid3, want: %f, got: %v", want, got)
	}
}
