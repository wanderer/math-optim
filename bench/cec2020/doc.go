// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

// Package cec2020 implements functions of the CEC2020 Testbed.
// Function definitions and overview: https://github.com/P-N-Suganthan/2020-Bound-Constrained-Opt-Benchmark/blob/d8b4c52f161562cd462e9b3352885e8df6fd2e41/Definitions%20of%20%20CEC2020%20benchmark%20suite%20Bound%20Constrained.pdf
// Example implementation: https://github.com/danney9512/CEC2020-Bound-Constrained-Opt_mpmL-SHADE/blob/master/mpmL-SHADE_source code/src/problem_test_functions.h
package cec2020
