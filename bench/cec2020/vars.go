// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package cec2020

import "git.dotya.ml/wanderer/math-optim/bench"

/*
extern double *OShift,*M,*y,*z,*x_bound;
extern int ini_flag,n_flag,func_flag,*SS;
*/

// Functions is a string-func map of function names and specific bench funcs
// for easier iterable access.
var Functions = map[string]func([]float64) float64{
	"Bent Cigar":           BentCigar,
	"Schwefel Modified":    Schwefel,
	"Lunacek Bi-Rastrigin": LunacekBiRastrigin,
	"Rosenbrock-Griewank":  RosenbrockGriewank,
	"Hybrid1":              Hybrid1,
	"Hybrid2":              Hybrid2,
	"Hybrid3":              Hybrid3,
	"Composition1":         Composition1,
	"Composition2":         Composition2,
	"Composition3":         Composition3,
}

// FuncNames represents a numbered list of function name.
var FuncNames = map[int]string{
	0: "Bent Cigar",
	1: "Schwefel Modified",
	2: "Lunacek Bi-Rastrigin",
	3: "Rosenbrock-Griewank",
	4: "Hybrid1",
	5: "Hybrid2",
	6: "Hybrid3",
	7: "Composition1",
	8: "Composition2",
	9: "Composition3",
}

// SearchRange specifies the CEC2020 search range.
var SearchRange = bench.NewfuncParams(searchRangeMin, searchRangeMax)

var (
	// Dimensions to bench.
	Dimensions = []int{
		5,
		10,
		15,
		20,
	}
	// MaxFES as specified for each dimension.
	MaxFES = []int{
		50000,
		1000000,
		3000000,
		10000000,
	}
)
