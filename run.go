// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"errors"
	"flag"
	"log"
	"runtime/pprof"
	"sync"

	"git.dotya.ml/wanderer/math-optim/algo"
	"git.dotya.ml/wanderer/math-optim/report"
)

var version = "development"

var (
	// ref: https://go.dev/blog/pprof.
	cpuprofile = flag.String("cpuprofile", "", "write cpu profile to this file")
	memprofile = flag.String("memprofile", "", "write memory profile to this file")

	doPrint  = flag.Bool("printreport", true, "print report.tex to console")
	generate = flag.Bool("generate", true, "run algos and generate plot pics/statistical tables (anew)")
	rS       = flag.Bool("randomsearch", false, "run Random Search algorithm")
	sHC      = flag.Bool("shc", false, "run Stochastic Hill Climbing algorithm")
	n100     = flag.Bool("N100", false, "run the \"100 Neighbours\" variant of SHC")

	// TODO(me): add flag for plot output format: -plotout=(svg,eps,pdf).

	jDE = flag.Bool("jde", false, "run Differential Evolution algorithm with parameter self adaptation")
	// run CEC2020 jDE by default.
	c2jDE     = flag.Bool("c2jde", true, "run CEC2020 version of the Differential Evolution algorithm with parameter self adaptation")
	c2SOMAT3A = flag.Bool("c2somat3a", false, "run CEC2020 version of the SOMA Team-to-Team Adaptive (T3A)")
)

func run() error {
	log.Println("starting math-optim version", "'"+version+"'")

	flag.Parse()

	err := profileCPU(cpuprofile)
	if err != nil {
		return err
	}

	// see profile.go on why we're calling this here.
	defer pprof.StopCPUProfile()

	if *generate {
		if !*jDE && !*c2jDE && !*c2SOMAT3A && !*sHC && !*rS {
			log.Println("at least one algo needs to be specified, exiting...")

			return errors.New("ErrNoAlgoSelected")
		}

		var wg sync.WaitGroup

		var m sync.Mutex

		if *jDE {
			wg.Add(1)

			go algo.DojDE(&wg, &m)
		}

		if *c2jDE {
			wg.Add(1)

			go algo.DoCEC2020jDE(&wg, &m)
		}

		if *c2SOMAT3A {
			wg.Add(1)

			go algo.DoCEC2020SOMAT3A(&wg, &m)
		}

		if *rS {
			wg.Add(1)

			go algo.DoRandomSearch(&wg, &m)
		}

		if *sHC {
			wg.Add(1)

			if *n100 {
				go algo.DoStochasticHillClimbing100Neigh(&wg, &m)
			} else {
				go algo.DoStochasticHillClimbing(&wg, &m)
			}
		}

		wg.Wait()

		var pL *report.PicList

		var benchCount int

		if *c2jDE && *c2SOMAT3A {
			pL, benchCount = algo.PrepCEC2020ComparisonOfMeans(&wg)
		} else {
			pL, benchCount = algo.PrepComparisonOfMeans(&wg)
		}

		report.SaveComparisonOfMeans(*pL, benchCount)
		report.SaveTexAllPics()
		report.SaveTexAllTables()
	}

	report.SaveAndPrint(*doPrint)

	err = profileMem(memprofile)
	if err != nil {
		return err
	}

	log.Println("looks like we're done")
	log.Println("run an equivalent of `pdflatex -clean -shell-escape -interaction=nonstopmode ./report.tex` to get a pdf")

	return nil
}
