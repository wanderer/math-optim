// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"os"
	"runtime/pprof"
)

// profileCPU checks whether CPU profiling was requested and if so, enables it.
// NOTE: deferred pprof.StopCPUProfile() needs to be called at the call site of
// this function, otherwise the profiling would end when this function goes out
// of scope.
func profileCPU(prof *string) error {
	if *prof != "" {
		f, err := os.Create(*prof)
		if err != nil {
			return err
		}

		err = pprof.StartCPUProfile(f)
		if err != nil {
			return err
		}
	}

	return nil
}

// profileMem checks if memory profiling was requested and if so, enables it.
func profileMem(prof *string) error {
	if *prof != "" {
		f, err := os.Create(*prof)
		if err != nil {
			return err
		}

		err = pprof.WriteHeapProfile(f)
		if err != nil {
			return err
		}
	}

	return nil
}
