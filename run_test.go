// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"flag"
	"testing"
)

func TestRun(t *testing.T) {
	t.Log("call run() (TODO: improve this test)")

	timeout := "1m10s"

	t.Log("set test timeout to", timeout)

	if err := flag.Set("test.timeout", timeout); err != nil {
		t.Errorf("failed to set timeout to %s", timeout)
	}

	if err := flag.Set("c2jde", "false"); err != nil {
		t.Errorf("failed to not run jDE: %q", err)
	}

	err := run()
	if err != nil {
		if err.Error() == "ErrNoAlgoSelected" {
			return
		}

		t.Error(err)
	}
}
