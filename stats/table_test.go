// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package stats

import (
	"testing"

	"git.dotya.ml/wanderer/math-optim/report"
)

func TestMakeRowTitle(t *testing.T) {
	want := `D=10, f=Schwefel, G=3000, I=30`
	got := makeRowTitle("Schwefel", 10, 3000, 30)

	if want != got {
		t.Errorf("wrong row title, want: %+q, got: %+q", want, got)
	}
}

func TestParseBenchStats(t *testing.T) {
	benchFuncStats := []FuncStats{
		{
			BenchName: "De Jong 5th",
			BenchResults: []BenchRound{
				{Iteration: 0, Results: []float64{2803.7015205977887, 2296.736381649773, 1763.9319525203364}},
				{Iteration: 1, Results: []float64{2169.7378893600176, 2169.7378893600176, 2169.7378893600176}},
				{Iteration: 2, Results: []float64{1909.6488355929007, 1336.3261400058473, 1336.3261400058473}},
				{Iteration: 3, Results: []float64{2227.0782796718786, 2227.0782796718786, 1681.0667739991388}},
			},
		},
	}
	testStats := []Stats{
		{
			Algo:           "Aladeen Search",
			Dimens:         5,
			BenchFuncStats: benchFuncStats,
			Iterations:     4,
			Generations:    3,
		},
	}

	wantResults := []float64{1336.3261400058473, 2169.7378893600176, 1737.765688971335, 1722.4993632597375, 342.37072192259393}
	want := report.Row{
		Title:  "D=5, f=De Jong 5th, G=3, I=4",
		Values: wantResults,
	}
	// expecting a singlerow so we're accessing it directly.
	got := parseSingleBenchStats(testStats)[0]

	if len(want.Values) != len(got.Values) {
		t.Errorf("outputs are of different sizes, want: %d, got: %d", len(want.Values), len(got.Values))
	}

	if want.Title != got.Title {
		t.Errorf("output titles differ, want: %q, got: %q", want.Title, got.Title)
	}

	for i := range want.Values {
		if want.Values[i] != got.Values[i] {
			t.Errorf("outputs don't match,\n\twant: %+v,\n\tgot: %+v", want, got)
		}
	}
}
