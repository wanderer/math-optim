// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package report

import (
	_ "embed"
	"log"
	"os"
	"regexp"
	"text/template"
	"time"
)

// picTexFiles holds paths to tex files including pics of a certain algo.
type picTexFiles struct {
	Algo      string
	FilePaths []string
}

// allThePics contains a slice of picTexFiles and consequently paths to all pic
// tex files generated during a single run of the program.
type allThePics struct {
	TexFiles []picTexFiles
}

var (
	allPics = &allThePics{}
	//go:embed allpics.tmpl
	tmplAllPicsFile []byte
)

// SaveTexAllPics feeds all paths of generated pics to a template that creates
// `allpics.tex` file, which then aggregates all tex files tracking plot pics
// in one place.
func SaveTexAllPics() {
	a := allPics
	texAllPicsFile := GetTexDir() + "allpics" + ".tex"
	r := regexp.MustCompile(`^Comparison of Algo Means`)
	tmplAllPics := template.New("allpics").Funcs(
		template.FuncMap{
			"isComparison": func(s string) bool {
				matches := r.FindAllString(s, -1)

				return matches != nil
			},
		},
	)
	tmplAllPics = template.Must(tmplAllPics.Parse(string(tmplAllPicsFile)))

	f, err := os.Create(texAllPicsFile)
	if err != nil {
		log.Println(err)
	}

	defer f.Close()

	err = tmplAllPics.Execute(f, struct {
		AllPics   allThePics
		Timestamp time.Time
	}{
		AllPics:   *a,
		Timestamp: time.Now(),
	})

	if err != nil {
		log.Println(err)
	}
}
