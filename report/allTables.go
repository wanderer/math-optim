// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package report

import (
	_ "embed"
	"log"
	"os"
	"text/template"
	"time"
)

type tableTexFiles struct {
	Algo      string
	FilePaths []string
}

type allTheTables struct {
	TexFiles []tableTexFiles
}

var (
	allTables = &allTheTables{}
	//go:embed alltables.tmpl
	tmplAllTablesFile []byte
)

func SaveTexAllTables() {
	a := allTables
	texAllTablesFile := GetTexDir() + "alltables" + ".tex"

	tmplAllTables := template.New("alltables")
	tmplAllTables = template.Must(tmplAllTables.Parse(string(tmplAllTablesFile)))

	f, err := os.Create(texAllTablesFile)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()

	err = tmplAllTables.Execute(f, struct {
		AllTables allTheTables
		Timestamp time.Time
	}{
		AllTables: *a,
		Timestamp: time.Now(),
	})

	if err != nil {
		log.Println(err)
	}
}
