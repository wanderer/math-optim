// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package report

import (
	_ "embed"
	"log"
	"os"
	"text/template"
	"time"

	"git.dotya.ml/wanderer/math-optim/util"
)

type Table struct {
	Algo      string
	Header    []string
	ColLayout []string
	Rows      []Row
}

type Row struct {
	// Title of the row should contain the settings used to get the Values.
	Title  string
	Values []float64
}

//go:embed table.tmpl
var tmplTableFile []byte

func NewTable() *Table {
	return &Table{}
}

func NewRow() *Row {
	return &Row{}
}

func SaveTableToFile(t Table) {
	safeName := util.SanitiseFName(t.Algo)
	texTableFile := GetTexDir() + "table-" + safeName + ".tex"
	tmplTable := template.New("table")

	tmplTable = template.Must(tmplTable.Parse(string(tmplTableFile)))

	ttf := tableTexFiles{Algo: t.Algo, FilePaths: []string{texTableFile}}
	allTables.TexFiles = append(allTables.TexFiles, ttf)

	f, err := os.Create(texTableFile)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()

	err = tmplTable.Execute(f, struct {
		Algo      string
		ColLayout []string
		ColNames  []string
		Rs        []Row
		Timestamp time.Time
	}{
		Algo:      t.Algo,
		ColNames:  t.Header,
		ColLayout: t.ColLayout,
		Rs:        t.Rows,
		Timestamp: time.Now(),
	})

	if err != nil {
		log.Println(err)
	}
}
