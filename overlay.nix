# heavily inspired by https://github.com/diamondburned/gotk4-nix/blob/d2bd6577f1867cb740b281baa48a895aed494967/overlay.nix
self: super: let
  patchelfer = arch: interpreter:
    super.writeShellScriptBin
    "${super.patchelf}/bin/patchelf --set-interpreter ${interpreter} \"$@\"";
in {
  go = super.go.overrideAttrs (old: {
    version = "1.18.2";
    src = builtins.fetchurl {
      url = "https://go.dev/dl/go1.18.2.src.tar.gz";
      sha256 = "sha256:1qk7as7a5wx3522ldsq8il1il9ic5xhbl6drg89r4h63l8zd0i1c";
    };
    doCheck = false;
    patches =
      (old.patches or [])
      ++ [
        # cmd/go/internal/work: concurrent ccompile routines
        (super.fetchpatch {
          url = "https://github.com/diamondburned/go/commit/ec3e1c9471f170187b6a7c83ab0364253f895c28.patch";
          sha256 = "sha256-KXP3gHRVHCVY60HnrLHv+QLib1hSYZOxsMjkbAiNi1g=";
        })
        # cmd/cgo: concurrent file generation
        (super.fetchpatch {
          url = "https://github.com/diamondburned/go/commit/50e04befeca9ae63296a73c8d5d2870b904971b4.patch";
          sha256 = "sha256-6PNUBlhGj53XS7f3dYOisDQ1PJldqwmWcgBnPE0cZsE=";
        })
        # the patches are also present locally for archival purposes
        # ./patches/diamondburned/go/ec3e1c9471f170187b6a7c83ab0364253f895c28.patch
        # ./patches/diamondburned/go/50e04befeca9ae63296a73c8d5d2870b904971b4.patch
        # TODO(me): don't forget to download the patch files when bumping the
        # urls above
      ];
  });

  # might as well be 'buildGoModule = super.buildGo118Module.override' but
  # we're overriding just the go package itself and leaving the rest of the
  # function alone. letting 'builGoModule' to be updated only with flake's
  # nixpkgs updates therefore seems to be the more reasonable option.
  buildGoModule = super.buildGoModule.override {
    # use the go we just built from sources as 'go' in the buildGoModule func
    inherit (self) go;
  };

  inherit (super) go-tools;

  # See https://sourceware.org/glibc/wiki/ABIList.
  patchelf-x86_64 = patchelfer "x86_64" "/lib64/ld-linux-x86-64.so.2";
  patchelf-aarch64 = patchelfer "aarch64" "/lib/ld-linux-aarch64.so.1";

  #dominikh = {
  #  go-tools = self.buildGoModule {
  #    name = "dominikh-go-tools";
  #    src = super.fetchFromGitHub {
  #      owner = "dominikh";
  #      repo = "go-tools";
  #      rev = "f4a2f64ce2386d1d392f2da44043c5ee3fb48216";
  #      sha256 = "1yhbz2sf332b6i00slsj4cn8r66x27kddw5vcjygkkiyny1a99qb";
  #    };
  #    vendorSha256 = "09jbarlbq47pcxy5zkja8gqvnqjp2mpbxnciv9lhilw9swqqwc0j";
  #    doCheck = false;
  #    subPackages = [ "cmd/staticcheck" ];
  #  };
  #};
}
