// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package util

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
)

// SanitiseFName clears spaces from the string that is supposed to be a file
// name so that the result is safer to use with e.g. latex.
func SanitiseFName(s string) string {
	return strings.ReplaceAll(s, " ", "--")
}

// CreateFolder creates a folder at the specified path, if it doesn't already
// exist.
func CreateFolder(path string) error {
	var err error

	if _, err = os.Stat(path); errors.Is(err, os.ErrNotExist) {
		printUtil("creating folder : " + path + " (if not exists)")

		err = os.Mkdir(path, os.ModePerm)
		if err != nil {
			log.Println(err)
		}
	}

	return err
}

// CreatePath creates all folders in the provided path, if they don't already
// exist.
func CreatePath(path string) error {
	var err error

	if _, err = os.Stat(path); errors.Is(err, os.ErrNotExist) {
		printUtil("creating path : " + path + " (if not exists)")

		err = os.MkdirAll(path, os.ModePerm)
		if err != nil {
			log.Println(err)
		}
	}

	return err
}

func getUtilLogPrefix() string {
	return " ***  util: "
}

func fmtUtilOut(input string) string {
	return getUtilLogPrefix() + input
}

func printUtil(input string) {
	if _, err := fmt.Fprintln(os.Stderr, fmtUtilOut(input)); err != nil {
		fmt.Fprintf(
			os.Stdout,
			getUtilLogPrefix(),
			"error while printing to stderr: %q\n * original message was: %q",
			err, input,
		)
	}
}
