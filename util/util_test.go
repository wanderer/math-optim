// Copyright 2023 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package util

import (
	"os"
	"testing"
)

// nolint: ifshort
func TestCreateFolder(t *testing.T) {
	// let's assume this will never exist in a clean project...
	testPath := "whatever-path"
	got := CreateFolder(testPath)

	var want error

	if want != got {
		t.Errorf("issue creating folder, testPath: %q, full cmd: %q", testPath, got.Error())
	}

	os.RemoveAll(testPath)
}

// nolint: ifshort
func TestCreatePath(t *testing.T) {
	// let's assume this, too, will never exist in a clean project...
	testPathRoot := "whatever-path"
	testPath := testPathRoot + "/whatever-subpath/subsubpath"
	got := CreatePath(testPath)

	var want error

	if want != got {
		t.Errorf("issue creating folders in path, testPath: %q, full cmd: %q", testPath, got.Error())
	}

	// clean up from the root.
	if err := os.RemoveAll(testPathRoot); err != nil {
		t.Error("error cleaning up: ", err)
	}
}

func TestSanitiseFname(t *testing.T) {
	want := "Stochastic--Hill--Climbing"
	got := SanitiseFName("Stochastic Hill Climbing")

	if want != got {
		t.Errorf("failed to sanitise file name, want: %s, got: %s", want, got)
	}
}
