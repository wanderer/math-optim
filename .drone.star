# Copyright 2023 wanderer <a_mirre at utb dot cz>
# SPDX-License-Identifier: GPL-3.0-or-later

def main(ctx):
	return [
		{
			"kind": "pipeline",
			"type": "docker",
			"name": "nix",
			"node": {
				"r": "w"
			},
			"steps": [
				{
					"name": "pull nix",
					"image": "docker.io/nixos/nix:2.8.0-amd64",
					"pull": "always",
					"depends_on": ["clone"],
					"volumes": [
						{
							"name": "nix",
							"path": "/nix"
						}
					],
					"commands": [
						"uname -r",
						"nix --version"
					]
				},
				{
					"name": "enable flakes",
					"image": "docker.io/nixos/nix:2.8.0-amd64",
					"pull": "if-not-exists",
					"depends_on": ["pull nix"],
					"volumes": [
						{
							"name": "nix",
							"path": "/nix"
						},
						{
							"name": "nixconf",
							"path": "/etc/nix"
						}
					],
					"commands": [
						"cat /etc/nix/nix.conf",
						"echo 'experimental-features = nix-command flakes' >> /etc/nix/nix.conf",
						"cat /etc/nix/nix.conf"
					]
				},
				{
					"name": "enable sandbox",
					"image": "docker.io/library/busybox",
					"pull": "always",
					"depends_on": ["enable flakes"],
					"volumes": [
						{
							"name": "nix",
							"path": "/nix"
						},
						{
							"name": "nixconf",
							"path": "/etc/nix"
						}
					],
					"commands": [
						"cat /etc/nix/nix.conf",
						"sed -i 's/^sandbox = false/sandbox = true/' /etc/nix/nix.conf",
						"cat /etc/nix/nix.conf"
					]
				},
				{
					"name": "set up cachix",
					"image": "docker.io/immawanderer/nix-cachix-glibc",
					"pull": "if-not-exists",
					"depends_on": ["enable sandbox"],
					"volumes": [
						{
							"name": "nixconf",
							"path": "/etc/nix"
						}
					],
					"commands": [
						"cat /etc/nix/nix.conf",
						"cachix use ${DRONE_REPO_NAME}",
						"cat /etc/nix/nix.conf"
					]
				},
				{
					"name": "nix-fmt",
					"image": "docker.io/nixos/nix:2.8.0-amd64",
					"pull": "always",
					"depends_on": ["enable sandbox"],
					"volumes": [
						{
							"name": "nixconf",
							"path": "/etc/nix"
						}
					],
					"commands": [
						"nix fmt -- --version",
						"nix fmt -- --check . || " +
							"(echo 'nix code unformatted! run \"nix fmt\" to fix it' && exit 1)"
					]
				},
				{
					"name": "statix",
					"image": "docker.io/immawanderer/nix-statix:linux-amd64",
					"pull": "if-not-exists",
					"depends_on": ["enable flakes"],
					"commands": [
						"statix --version",
						"statix check ."
					]
				},
				{
					"name": "nix flake check",
					"image": "docker.io/nixos/nix:2.8.0-amd64",
					"pull": "if-not-exists",
					"depends_on": ["statix", "enable sandbox"],
					"volumes": [
						{
							"name": "nixconf",
							"path": "/etc/nix"
						}
					],
					"commands": [
						"nix flake check --keep-going"
					]
				},
				{
					"name": "nix flake show",
					"image": "docker.io/nixos/nix:2.8.0-amd64",
					"pull": "if-not-exists",
					"depends_on": ["nix flake check"],
					"volumes": [
						{
							"name": "nixconf",
							"path": "/etc/nix"
						}
					],
					"commands": [
						"nix flake show"
					]
				},
				{
					"name": "nix flake metadata",
					"image": "docker.io/nixos/nix:2.8.0-amd64",
					"pull": "if-not-exists",
					"depends_on": ["nix flake check"],
					"volumes": [
						{
							"name": "nixconf",
							"path": "/etc/nix"
						}
					],
					"commands": [
						"nix flake metadata"
					]
				},
				{
					"name": "go test",
					"image": "docker.io/nixos/nix:2.8.0-amd64",
					"pull": "if-not-exists",
					"depends_on": ["set up cachix"],
					"volumes": [
						{
							"name": "nix",
							"path": "/nix"
						},
						{
							"name": "nixconf",
							"path": "/etc/nix"
						},
						{
							"name": "gopath",
							"path": "/root/go"
						}
					],
					"commands": [
						"nix develop --command go test -cover ./..."
					]
				},
				{
					"name": "nix build",
					"image": "docker.io/nixos/nix:2.8.0-amd64",
					"pull": "if-not-exists",
					"depends_on": ["nix-fmt", "set up cachix", "nix flake check"],
					"volumes": [
						{
							"name": "nix",
							"path": "/nix"
						},
						{
							"name": "nixconf",
							"path": "/etc/nix"
						},
						{
							"name": "gopath",
							"path": "/root/go"
						}
					],
					"commands": [
						"nix build .#${DRONE_REPO_NAME}"
					]
				},
				{
					"name": "ldd",
					"image": "docker.io/nixos/nix:2.8.0-amd64",
					"pull": "if-not-exists",
					"depends_on": ["nix build"],
					"volumes": [
						{
							"name": "nix",
							"path": "/nix"
						},
						{
							"name": "nixconf",
							"path": "/etc/nix"
						}
					],
					"commands": [
						"nix develop --command ldd result/bin/${DRONE_REPO_NAME} || true"
					]
				}
			],
			"volumes": [
				{
					"name": "nix",
					"temp": {}
				},
				{
					"name": "nixconf",
					"temp": {}
				},
				{
					"name": "gopath",
					"temp": {}
				}
			]
		},
		{
			"kind": "pipeline",
			"type": "docker",
			"name": "golangci-lint|godocs",
			"steps": [
				{
					"name": "golangci-lint",
					"image": "docker.io/library/golang:1.18.5-alpine3.16",
					"pull": "always",
					"environment": {
						"CGO_ENABLED": "0",
					},
					"commands": [
						"go install -v "+
						"github.com/golangci/golangci-lint/cmd/golangci-lint@"+
						"v1.50.1",
						"golangci-lint version",
						"golangci-lint run -v --timeout 5m"
					],
				},
				{
					"name": "goreportcard refresh",
					"image": "docker.io/curlimages/curl:7.83.1",
					"pull": "if-not-exists",
					"depends_on": ["golangci-lint"],
					"trigger": {
						"ref": {
							"include": [
								"refs/tags/**",
								"refs/heads/development"
							],
							"exclude": [
								"refs/pull/**"
							]
						}
					},
					"commands": [
						"uname -r",
						"curl --version",
						"curl " +
							"-sS " +
							"-X POST " +
							"-F \"repo=git.dotya.ml/${DRONE_REPO}\" " +
							"https://goreportcard.com/checks " +
							"-o /dev/null"
					]
				},
				{
					"name": "godocs refresh",
					"image": "docker.io/curlimages/curl:7.83.1",
					"pull": "if-not-exists",
					"depends_on": ["golangci-lint"],
					"trigger": {
						"ref": {
							"include": [
								"refs/tags/**",
								"refs/heads/development"
							],
							"exclude": [
								"refs/pull/**"
							]
						}
					},
					"commands": [
						"uname -r",
						"curl --version",
						"curl " +
							"-sS " +
							"-X POST " +
							"https://godocs.io/git.dotya.ml/${DRONE_REPO}/refresh " +
							"-o /dev/null"
					]
				}
			]
		}
	]

# vim:ft=bzl.starlark:syntax=bzl.starlark:noexpandtab:ts=4:sts=4:sw=4:foldmethod=manual
