augroup vimgo_local
	autocmd!

	" quality of life improvements, some as per
	" https://github.com/fatih/vim-go/wiki/Tutorial#vimrc-improvements-1
	" run :GoBuild or :GoTestCompile based on the go file
	function! s:build_go_files()
		let l:file = expand('%')
		if l:file =~# '^\f\+_test\.go$'
			call go#test#Test(0, 1)
		elseif l:file =~# '^\f\+\.go$'
			call go#cmd#Build(0)
		endif
	endfunction

	autocmd FileType go nmap <leader>b :<C-u>call <SID>build_go_files()<CR>
	autocmd FileType go nmap <leader>gb :<C-u>call <SID>build_go_files()<CR>

	autocmd FileType go nmap <leader>gc <Plug>(go-coverage-toggle)

	let g:go_gopls_gofumpt=1
	let g:go_fmt_autosave=1
	let g:go_highlight_types = 1
	let g:go_fmt_command='gofumpt'
	let g:go_test_timeout = '10s'
	let g:go_highlight_build_constraints = 1

	let g:go_metalinter_command = 'golangci-lint run'
	let g:go_metalinter_autosave = 1
	let g:go_metalinter_deadline = '3s'
augroup END
